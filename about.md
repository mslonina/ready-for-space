---
title: About
date: 2017-11-01 03:00:00 +0000
banner_image: ''
heading: 'About '
sub_heading: About Ready for Space
layout: landing-page
textline: We are ready for space. Seriously.
publish_date: 2017-12-01 04:00:00 +0000
show_staff: true
menu:
  footer:
    identifier: _about
    weight: 3
  navigation:
    identifier: _about
    weight: 2

---
Hey! We are ready for space.